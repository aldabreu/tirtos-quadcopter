################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
sensors/SensorI2C.obj: ../sensors/SensorI2C.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"D:/ti/ccsv6/tools/compiler/arm_5.2.8/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -O4 --fp_mode=relaxed --opt_for_speed=5 --include_path="D:/ti/ccsv6/ccs_base/arm/include" --include_path="D:/ti/ccsv6/ccs_base/arm/include/CMSIS" --include_path="D:/ti/CCS_Workspaces/Quadcopter/Quadcopter" --include_path="D:/ti/tirtos_msp43x_2_20_00_06/products/msp432_driverlib_3_21_00_05/inc/CMSIS" --include_path="D:/ti/tirtos_msp43x_2_20_00_06/products/msp432_driverlib_3_21_00_05/inc" --include_path="D:/ti/tirtos_msp43x_2_20_00_06/products/msp432_driverlib_3_21_00_05/driverlib/MSP432P4xx" --include_path="D:/ti/ccsv6/tools/compiler/arm_5.2.8/include" --advice:power_severity=suppress --advice:power="all" -g --gcc --define=__MSP432P401R__ --define=ccs --define=MSP432WARE --display_error_number --diag_warning=225 --diag_warning=255 --diag_wrap=off --gen_func_subsections=on --preproc_with_compile --preproc_dependency="sensors/SensorI2C.pp" --obj_directory="sensors" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sensors/SensorMpu9250.obj: ../sensors/SensorMpu9250.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"D:/ti/ccsv6/tools/compiler/arm_5.2.8/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -O4 --fp_mode=relaxed --opt_for_speed=5 --include_path="D:/ti/ccsv6/ccs_base/arm/include" --include_path="D:/ti/ccsv6/ccs_base/arm/include/CMSIS" --include_path="D:/ti/CCS_Workspaces/Quadcopter/Quadcopter" --include_path="D:/ti/tirtos_msp43x_2_20_00_06/products/msp432_driverlib_3_21_00_05/inc/CMSIS" --include_path="D:/ti/tirtos_msp43x_2_20_00_06/products/msp432_driverlib_3_21_00_05/inc" --include_path="D:/ti/tirtos_msp43x_2_20_00_06/products/msp432_driverlib_3_21_00_05/driverlib/MSP432P4xx" --include_path="D:/ti/ccsv6/tools/compiler/arm_5.2.8/include" --advice:power_severity=suppress --advice:power="all" -g --gcc --define=__MSP432P401R__ --define=ccs --define=MSP432WARE --display_error_number --diag_warning=225 --diag_warning=255 --diag_wrap=off --gen_func_subsections=on --preproc_with_compile --preproc_dependency="sensors/SensorMpu9250.pp" --obj_directory="sensors" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sensors/SensorUtil.obj: ../sensors/SensorUtil.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"D:/ti/ccsv6/tools/compiler/arm_5.2.8/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 --abi=eabi -me -O4 --fp_mode=relaxed --opt_for_speed=5 --include_path="D:/ti/ccsv6/ccs_base/arm/include" --include_path="D:/ti/ccsv6/ccs_base/arm/include/CMSIS" --include_path="D:/ti/CCS_Workspaces/Quadcopter/Quadcopter" --include_path="D:/ti/tirtos_msp43x_2_20_00_06/products/msp432_driverlib_3_21_00_05/inc/CMSIS" --include_path="D:/ti/tirtos_msp43x_2_20_00_06/products/msp432_driverlib_3_21_00_05/inc" --include_path="D:/ti/tirtos_msp43x_2_20_00_06/products/msp432_driverlib_3_21_00_05/driverlib/MSP432P4xx" --include_path="D:/ti/ccsv6/tools/compiler/arm_5.2.8/include" --advice:power_severity=suppress --advice:power="all" -g --gcc --define=__MSP432P401R__ --define=ccs --define=MSP432WARE --display_error_number --diag_warning=225 --diag_warning=255 --diag_wrap=off --gen_func_subsections=on --preproc_with_compile --preproc_dependency="sensors/SensorUtil.pp" --obj_directory="sensors" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


