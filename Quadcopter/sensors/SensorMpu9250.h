/*
 * Copyright (c) 2015-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/** ============================================================================
 *  @file       SensorMpu9250.h
 *
 *  @brief      Driver for the InvenSense MPU9250 Motion Processing Unit.
 *
 *  ============================================================================
 */
#ifndef SENSOR_MPU9250_H
#define SENSOR_MPU9250_H

#ifdef __cplusplus
extern "C" {
#endif

/* -----------------------------------------------------------------------------
 *                                          Includes
 * -----------------------------------------------------------------------------
 */
#include "stdint.h"
#include "stdbool.h"

/* -----------------------------------------------------------------------------
 *                                          Constants
 * -----------------------------------------------------------------------------
 */
// Accelerometer ranges
#define ACC_RANGE_2G                0
#define ACC_RANGE_4G                1
#define ACC_RANGE_8G                2
#define ACC_RANGE_16G               3
#define ACC_RANGE_INVALID           0xFF

// Accelerometer DLPF Bandwidth settings
#define ACC_BAND_460HZ_FS_1KHZ      0
#define ACC_BAND_184HZ_FS_1KHZ      1
#define ACC_BAND_92HZ_FS_1KHZ       2
#define ACC_BAND_41HZ_FS_1KHZ       3
#define ACC_BAND_20HZ_FS_1KHZ       4
#define ACC_BAND_10HZ_FS_1KHZ       5
#define ACC_BAND_5HZ_FS_1KHZ        6
#define ACC_BAND_1130HZ_FS_4KHZ     7

// Gyroscope ranges
#define GYRO_RANGE_250DPS           0
#define GYRO_RANGE_500DPS           1
#define GYRO_RANGE_1000DPS          2
#define GYRO_RANGE_2000DPS          3
#define GYRO_RANGE_INVALID          0xFF

// Gyroscope DLPF Bandwidth settings

#define GYRO_BAND_250HZ_FS_8KHZ     0
#define GYRO_BAND_184HZ_FS_1KHZ     1
#define GYRO_BAND_92HZ_FS_1KHZ      2
#define GYRO_BAND_41HZ_FS_1KHZ      3
#define GYRO_BAND_20HZ_FS_1KHZ      4
#define GYRO_BAND_10HZ_FS_1KHZ      5
#define GYRO_BAND_5HZ_FS_1KHZ       6
#define GYRO_BAND_3600HZ_FS_8KHZ    7
#define GYRO_BAND_8800HZ_FS_32KHZ   8
#define GYRO_BAND_3600HZ_FS_32KHZ   9

#define MPU_AX_GYR                  0x07
#define MPU_AX_ACC                  0x38
#define MPU_AX_MAG                  0x40
#define MPU_AX_ALL                  0x7F
            
// Interrupt status bit         
#define MPU_DATA_READY              0x01
#define MPU_MOVEMENT                0x40
            
// Magnetometer status          
#define MAG_STATUS_OK               0x00
#define MAG_READ_ST_ERR             0x01
#define MAG_DATA_NOT_RDY            0x02
#define MAG_OVERFLOW                0x03
#define MAG_READ_DATA_ERR           0x04
#define MAG_BYPASS_FAIL             0x05
#define MAG_NO_POWER                0x06

// Bit values for MPU interrupts
#define BIT_RAW_RDY                 0x01
#define BIT_FSYNC                   0x08
#define BIT_ANY_RD_CLR              0x10
#define BIT_FIFO_OFLOW              0x10
#define BIT_WOM                     0x40
 
// Bit values for MPU int pin configuration
#define BIT_ACTL                    0x80
#define BIT_OPEN                    0x40
#define BIT_LATCH_EN                0x20
#define BIT_INT_ANYRD_2CLEAR        0x10
#define BIT_ACTL_FSYNC              0x04
#define BIT_BYPASS_EN               0x02

#define BITSET                      true
#define BITCLEAR                    false

/* ----------------------------------------------------------------------------
 *                                           Typedefs
 * -----------------------------------------------------------------------------
*/
typedef void (*SensorMpu9250CallbackFn_t)(void);

/* -----------------------------------------------------------------------------
 *                                          Functions
 * -----------------------------------------------------------------------------
 */
bool SensorMpu9250_init(void);
bool SensorMpu9250_reset(void);
void SensorMpu9250_registerCallback(SensorMpu9250CallbackFn_t);
bool SensorMpu9250_test(void);
void SensorMpu9250_powerOn(void);
void SensorMpu9250_powerOff(void);
bool SensorMpu9250_powerIsOn(void);

void SensorMpu9250_enable(uint16_t config);
bool SensorMpu9250_enableWom(uint8_t threshold);
bool SensorMpu9250_irqPinConfig(uint8_t flags, bool bitSet);
bool SensorMpu9250_irqConfig(uint8_t flags, bool bitSet);
uint8_t SensorMpu9250_irqStatus(void);
bool SensorMpu9250_irqClear(uint8_t flags);

bool SensorMpu9250_setSamplingRate(uint16_t sampleRate);

bool SensorMpu9250_accSetDLPF(uint8_t bandwidth);
bool SensorMpu9250_accSetRange(uint8_t range);
uint8_t SensorMpu9250_accReadRange(void);
bool SensorMpu9250_accRead(int16_t *rawData);
float SensorMpu9250_accConvert(int16_t rawValue);

bool SensorMpu9250_gyroSetDLPF(uint8_t bandwidth);
bool SensorMpu9250_gyroSetRange(uint8_t range);
uint8_t SensorMpu9250_gyroReadRange(void);
bool SensorMpu9250_gyroRead(int16_t *rawData);
float SensorMpu9250_gyroConvert(int16_t rawValue);

bool SensorMpu9250_magTest(void);
uint8_t SensorMpu9250_magRead(int16_t *pRawData);
uint8_t SensorMpu9250_magStatus(void);
void SensorMpu9250_magReset(void);
bool SensorMpu9250_AuxMagEnable(bool enable);
uint8_t SensorMpu9250_magAuxRead(int16_t *pRawData);
float SensorMpu9250_magConvert(int16_t rawValue);
bool sensorMpu9250I2CMstReset(void);





/*******************************************************************************
*/

#ifdef __cplusplus
};
#endif

#endif
