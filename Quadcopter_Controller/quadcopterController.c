/*
 * Copyright (c) 2015-2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== quadcopter_Controller.c ========
 */
#include <math.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Mailbox.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>

/* TI-RTOS Header files */
#include <ti/drivers/rf/RF.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/ADC.h>
#include <ti/drivers/pin/PINCC26XX.h>
#define Display_DISABLE_ALL 1

#include <ti/mw/display/Display.h>

/* Example/Board Header files */
#include "Board.h"
/* EasyLink API Header files */
#include "easylink/EasyLink.h"
#define RFEASYLINKTX_BURST_SIZE         10
#define RFEASYLINKTXPAYLOAD_LENGTH      11

#define TASKSTACKSIZE (1024)
#define DEBOUNCEMS (300)
#define SAMPLINGPERIODMS (16)
/* Amount of thrust to increase/decrease each time the joystick is positive/negative */
#define THRUSTINC (10)
#define MSINASEC (1000)
#define USINAMS (1000)
#define DEBOUNCETICKS ((DEBOUNCEMS * USINAMS) / (Clock_tickPeriod))
#define SAMPLERATETICKS ((SAMPLINGPERIODMS * USINAMS) / (Clock_tickPeriod))

/* RC Channels */
typedef enum RCCHANNELS {
	JOYLEFTX = 0,
	JOYLEFTY,
	JOYRIGHTX,
	JOYRIGHTY,
	BUTTONPRESSED,

	NUMCHANNELS
} RCCHANNELS;

typedef enum RFPKT {
    BUTTONSTATE = 0,
    THRUSTMSB,
    THRUSTLSB,
    PITCHMSB,
    PITCHLSB,
    ROLLMSB,
    ROLLLSB,
    YAWMSB,
    YAWLSB,

    PAYLOADLENGTH
} RFPKT;

/* Controller State */
typedef enum ControllerState {
    RCIDLE = 0, /* Initial state, no drivers active */
    RCCALIBRATING, /* Currently taking adc samples to find controller zero offsets */
    RCACTIVE /* Currently transmitting adjusted controller values */
} ControllerState;

/* Current controller state */
uint8_t currState = RCIDLE;
/* Data packet interval is equal to the sampling period */

typedef struct adcSampleObj {
    uint16_t adcChannel[NUMCHANNELS];
} adcSampleObj, *adcSample;

#define EXTRACTMSB16(X) ((X >> 8) & (0xFF))
#define EXTRACTLSB16(X) (X & 0xFF)
/* Takes two uint8_t's (LSB and MSB respectively and combines them into a int16_t) */
#define UINT8TOINT16(MSB, LSB) ((MSB << 8) | LSB)

#define NUMMBXELEMS (15)
#define NUMCALIBSAMPLES (500) /* Number of samples to use for calibrating joysticks */
#define MAILBOXSIZE ((NUMMBXELEMS) * (sizeof(Mailbox_MbxElem) + sizeof(adcSampleObj)))

#define MINTHRUST (1000)
#define MAXTHRUST (2000)
#define MINROLL (-45)
#define MAXROLL (45)
#define MAXPITCH (45)
#define MINPITCH (-45)
#define MINYAW (-150)
#define MAXYAW (150)

/* UART Packet defines */
#define STARTFLAG 0x12
#define ENDFLAG 0x13
#define ESCFLAG 0x7D
#define OUTPUTPACKETLEN (2 * PAYLOADLENGTH + 2)

/* Pin driver handles */
static PIN_Handle buttonPinHandle;
static PIN_Handle ledPinHandle;
Display_Handle hDisplaySerial;

/* Global memory storage for a PIN_Config table */
static PIN_State buttonPinState;
static PIN_State ledPinState;

Task_Struct task0Struct, task1Struct, task2Struct;
Char task0Stack[TASKSTACKSIZE], task1Stack[TASKSTACKSIZE],
    task2Stack[TASKSTACKSIZE];

Semaphore_Struct sampleStruct, calibStruct, rfRdyStruct;
Semaphore_Handle sampleSem, calibSem, rfRdySem;

Clock_Struct clk0Struct, clk1Struct, clk2Struct;
Clock_Handle clk0Handle, clk1Handle, clk2Handle;

Mailbox_Struct mbxStruct;
Mailbox_Handle mbxHandle;
uint8_t mailboxBuf[MAILBOXSIZE];

/* Max and Min for left and right joystick */
int16_t leftXMax, leftXMin, leftYMax, leftYMin, rightXMax,
	rightXMin, rightYMax, rightYMin;
int16_t leftXAvg, leftYAvg, rightXAvg, rightYAvg;

volatile uint8_t buttonPressed = 0;
/*
 * Initial LED pin configuration table
 *   - LEDs Board_LED0 is on.
 *   - LEDs Board_LED1 is off.
 */
PIN_Config ledPinTable[] = {
    Board_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH  | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

/*
 * Application button pin configuration table:
 *   - Buttons interrupts are configured to trigger on falling edge.
 */
PIN_Config buttonPinTable[] = {
    Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    Board_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};

/* Function prototypes */
void buttonCallbackFxn(PIN_Handle handle, PIN_Id pinId);
float mapInputToOutput(float input, float inputStart, float inputEnd,
        float outputStart, float outputEnd);
Void controllerFxn(UArg arg0, UArg arg1);
Void calibrationFxn(UArg arg0, UArg arg1);
Void RFTransmitFxn(UArg arg0, UArg arg1);
Void gpioDebounceFxn0(UArg arg0);
Void gpioDebounceFxn1(UArg arg0);
Void periodicSampleFxn(UArg arg0);
bool isInRange(int16_t val1, int16_t val2, int16_t range);
void printData(adcSampleObj adcData);

bool isInRange(int16_t val1, int16_t val2, int16_t range)
{
    if (fabs(val1 - val2) < range) {
        return (true);
    }
    return (false);
}

/* Map an input value with range [inputStart, inputEnd] to a new output within the range [outputStart, ouputEnd] */
float mapInputToOutput(float input, float inputStart, float inputEnd,
        float outputStart, float outputEnd)
{
    float slope;
    slope = (outputEnd - outputStart) / ((1.0)*(inputEnd - inputStart));
    
    return ((outputStart + slope * (input - inputStart) * (1.0)));
}

/* 
 * Stuff an input array with:
 * START(0x12) and END(0x13) flags and ESC(0x7D) characters
 * Packet format: START | DATA | DATA | ... | ESC | DATA(FLAG/ESC) | END
 * Note: The output array must be at least 2 * len + 2 elements wide.
 */

void byteStuff(uint8_t *input, uint8_t len, uint8_t *output)
{
    uint8_t inputIndex, outputIndex;

    outputIndex = 0;
    output[outputIndex++] = STARTFLAG;

    /* Escape any flags in the input data stream */
    for (inputIndex = 0; inputIndex < len; inputIndex++) {
        if ((input[inputIndex] == STARTFLAG) ||
            (input[inputIndex] == ENDFLAG) ||
            (input[inputIndex] == ESCFLAG)) {

            output[outputIndex++] = ESCFLAG;
        }
        output[outputIndex++] = input[inputIndex];
    }

    output[outputIndex] = ENDFLAG;
}

void byteUnStuff(uint8_t *input, uint8_t *output)
{
    uint8_t inputIndex, outputIndex;

    /* Ignore STARTFLAG */
    inputIndex = 0;
    outputIndex = 0;

    while (input[inputIndex] != ENDFLAG) {
            if (input[inputIndex] == ESCFLAG) {
            /* Ignore the ESC flag and take the next byte */
            inputIndex++;
        }
        output[outputIndex++] = input[inputIndex++];
    }
}

/*
 *  ======== RFTransmitFxn ========
 */
Void RFTransmitFxn(UArg arg0, UArg arg1)
{
    adcSampleObj adcData;
    uint8_t i;
    /*
     * [BUTTONSTATE(uint8_t), THRUSTMSB, THRUSTLSB, PITCHMSB, PITCHLSB, ROLLMSB,
     *  ROLLLSB, YAWMSB, YAWLSB]
     */
    uint8_t joystickData[PAYLOADLENGTH];

    /* Signal failure if data if 5 samples are missed */
    uint16_t timeout = 5 * SAMPLERATETICKS;
    int16_t rcThrust;
    float rcPitch, rcRoll, rcYaw;
	uint8_t currVal;
    EasyLink_TxPacket txPacket =  { {0}, 0, 0, {0} };

    /* Zero offset adjusted joystick values */
    int16_t adjJoyLeftX, adjJoyLeftY, adjJoyRightX, adjJoyRightY;

    /* Initialize RF Driver */
    EasyLink_init(EasyLink_Phy_50kbps2gfsk);
    /* Set Freq to 868MHz */
    EasyLink_setFrequency(868000000);
    /* Set output power to 12dBm */
    EasyLink_setRfPwr(12);


    while (1) {
        if ((currState == RCCALIBRATING) || (currState == RCIDLE)){
            Semaphore_pend(rfRdySem, BIOS_WAIT_FOREVER);

            /* Update state to transmitting */
            currState = RCACTIVE;

            /* Set initial thrust to 1000 (All mototrs off) */
            rcThrust = 1000;

            /* Begin Retrieving Data */
            Clock_start(clk0Handle);
        }

        /* Get ADC data */
        if (!Mailbox_pend(mbxHandle, &adcData, timeout)) {
            System_printf("Error data aquisition failure\n");
            System_flush();
            while(1);
        }

        /* Map ADC values to controller thrust, pitch, roll and yaw */
        /* Ranges: Thrust(1000-2000), Yaw(+- 150 degrees), pitch/roll(+- 45 degrees) */

        adjJoyLeftX = adcData.adcChannel[JOYLEFTX] - leftXAvg;
        adjJoyLeftY = adcData.adcChannel[JOYLEFTY] - leftYAvg;
        adjJoyRightX = adcData.adcChannel[JOYRIGHTX] - rightXAvg;
        adjJoyRightY = adcData.adcChannel[JOYRIGHTY] - rightYAvg;

        /*System_printf("LeftX: %d LeftY: %d RightX: %d RightY: %d\n",
        		adjJoyLeftX, adjJoyLeftY, adjJoyRightX, adjJoyRightY);*/

        /* Allow for 10% error when joysticks for "zero" state */
        if (isInRange(adjJoyLeftX, 0, leftXMax / 10)) {
            adjJoyLeftX = 0;
        }
        if (isInRange(adjJoyRightX, 0, rightXMax / 10)) {
            adjJoyRightX = 0;
        }
        if (isInRange(adjJoyRightY, 0, rightYMax / 10)) {
        	adjJoyRightY = 0;
        }

        /* Thrust needs to be a state variable(To hold position) */
        if ((adjJoyLeftY < 0) && (!isInRange(adjJoyLeftY, 0, leftYMax / 10))) {
            if (rcThrust > MINTHRUST) {
                rcThrust -= THRUSTINC; 
            }
        }
        else if (!isInRange(adjJoyLeftY, 0, leftYMax / 10)) {
            if (rcThrust < MAXTHRUST) {
                rcThrust += THRUSTINC; 
            }
        }

        rcYaw = mapInputToOutput(adjJoyLeftX, leftXMin, leftXMax,
            MINYAW, MAXYAW);

        rcPitch = mapInputToOutput(adjJoyRightY, rightYMin, rightYMax,
            MINPITCH, MAXPITCH);

        rcRoll = mapInputToOutput(adjJoyRightX, rightXMin, rightXMax, MINROLL,
            MAXROLL);
/*
        System_printf("Pitch: %f Roll: %f Yaw: %f\n", rcPitch, rcRoll, rcYaw);
        System_flush();*/

        /* Prepare data packet */
        joystickData[BUTTONSTATE] = buttonPressed;
        joystickData[THRUSTMSB] = EXTRACTMSB16((int16_t)rcThrust);
        joystickData[THRUSTLSB] = EXTRACTLSB16((int16_t)rcThrust);
        joystickData[PITCHMSB] = EXTRACTMSB16((int16_t)rcPitch);
        joystickData[PITCHLSB] = EXTRACTLSB16((int16_t)rcPitch);
        joystickData[ROLLMSB] = EXTRACTMSB16((int16_t)rcRoll);
        joystickData[ROLLLSB] = EXTRACTLSB16((int16_t)rcRoll);
        joystickData[YAWMSB] = EXTRACTMSB16((int16_t)rcYaw);
        joystickData[YAWLSB] = EXTRACTLSB16((int16_t)rcYaw);

#define UINT8TOINT16(MSB, LSB) ((MSB << 8) | LSB)

/*int16_t rcData[5];

        rcData[0] = (int16_t)joystickData[BUTTONSTATE];
        rcData[1] = UINT8TOINT16((int16_t)joystickData[THRUSTMSB],
            (int16_t)joystickData[THRUSTLSB]);
        rcData[2] = UINT8TOINT16((int16_t)joystickData[PITCHMSB],
            (int16_t)joystickData[PITCHLSB]);
        rcData[3] = UINT8TOINT16((int16_t)joystickData[ROLLMSB],
            (int16_t)joystickData[ROLLLSB]);
        rcData[4] = UINT8TOINT16((int16_t)joystickData[YAWMSB],
            (int16_t)joystickData[YAWLSB]);

rcThrust = rcData[1];
rcRoll = rcData[3];
rcPitch = rcData[2];
rcYaw = rcData[4];*/


		Display_print5(hDisplaySerial, 0, 0, "InputBTN: %d RCThrust: %d RCPitch: %d RCRoll: %d RCYaw: %d\n",
				buttonPressed, rcThrust, rcPitch, rcRoll, rcYaw);

        /* Reset button state */
        buttonPressed = 0x00;
        
        /* Create packet */
        byteStuff(joystickData, PAYLOADLENGTH, txPacket.payload);

        txPacket.len = OUTPUTPACKETLEN;
        txPacket.dstAddr[0] = 0xaa;

        /* Set Tx absolute time to current time + 20ms */
        txPacket.absTime = 0;

        EasyLink_Status result = EasyLink_transmit(&txPacket);

        if (result == EasyLink_Status_Success)
        {
            PIN_setOutputValue(ledPinHandle, Board_LED1,!PIN_getOutputValue(Board_LED1));
        }
        else
        {
            /* Toggle LED1 and LED2 to indicate error */
            PIN_setOutputValue(ledPinHandle, Board_LED1,!PIN_getOutputValue(Board_LED0));
            PIN_setOutputValue(ledPinHandle, Board_LED2,!PIN_getOutputValue(Board_LED1));
        }
    }
}

void printData(adcSampleObj adcData)
{
    System_printf("LEFTX: %d LEFTY: %d RIGHTX: %d RIGHTY:%d\n",
        adcData.adcChannel[JOYLEFTX], adcData.adcChannel[JOYLEFTY],
        adcData.adcChannel[JOYRIGHTX], adcData.adcChannel[JOYRIGHTY]);
    System_flush();
}

/*
 *  ======== calibrationFxn ========
 *  Unblocked when button0 is pushed, calibrates the controller by
 *  finding the min/max values for each stick.
 */

Void calibrationFxn(UArg arg0, UArg arg1)
{
    adcSampleObj adcData;
    uint16_t dataCnt;

    /* Initialize display and try to open UART */
    Display_Params params;
    Display_Params_init(&params);

    hDisplaySerial = Display_open(Display_Type_UART, &params);

    /* Check if the selected Display type was found and successfully opened */
    if (hDisplaySerial) {
        Display_print0(hDisplaySerial, 0, 0, "Hello Serial!");
    }

    /* Signal failure if data if 5 samples are missed */
    uint16_t timeout = 5 * SAMPLERATETICKS;

    while (1) {
        Semaphore_pend(calibSem, BIOS_WAIT_FOREVER);

    	/* Initialize Max/Min values */
    	leftXMax = 0x00;
    	leftXMin = 0xffff;
    	leftYMax = 0x00;
    	leftYMin = 0xffff;
    	rightXMax = 0x00;
    	rightXMin = 0xffff;
    	rightYMax = 0x00;
    	rightYMin = 0xffff;

        /* Update state */
        currState = RCCALIBRATING;

        /* Begin Retrieving Data */
        Clock_start(clk0Handle);

        for (dataCnt = 0; dataCnt < NUMCALIBSAMPLES; dataCnt++) {
            if (!Mailbox_pend(mbxHandle, &adcData, BIOS_WAIT_FOREVER)) {
                System_printf("Error data aquisition failure\n");
                System_flush();
                while(1);
            }

            //printData(adcData);

            /* Left X Max/Min */
            if (adcData.adcChannel[JOYLEFTX] > leftXMax) {
            	leftXMax = adcData.adcChannel[JOYLEFTX];
            }
            if (adcData.adcChannel[JOYLEFTX] < leftXMin) {
                leftXMin = adcData.adcChannel[JOYLEFTX];
            }

            /* Left Y Max/Min */
            if (adcData.adcChannel[JOYLEFTY] > leftYMax) {
            	leftYMax = adcData.adcChannel[JOYLEFTY];
            }
            if (adcData.adcChannel[JOYLEFTY] < leftYMin) {
                leftYMin = adcData.adcChannel[JOYLEFTY];
            }

            /* Right X Max/Min */
            if (adcData.adcChannel[JOYRIGHTX] > rightXMax) {
            	rightXMax = adcData.adcChannel[JOYRIGHTX];
            }
            if (adcData.adcChannel[JOYRIGHTX] < rightXMin) {
            	rightXMin = adcData.adcChannel[JOYRIGHTX];
            }

            /* Right Y Max/Min */
            if (adcData.adcChannel[JOYRIGHTY] > rightYMax) {
            	rightYMax = adcData.adcChannel[JOYRIGHTY];
            }
            if (adcData.adcChannel[JOYRIGHTY] < rightYMin) {
                rightYMin = adcData.adcChannel[JOYRIGHTY];
            }
        }

        /* Indicate end of calibration */
        PIN_setOutputValue(ledPinHandle, Board_LED0,!PIN_getOutputValue(Board_LED0));
        Task_sleep(100000);
        PIN_setOutputValue(ledPinHandle, Board_LED0,!PIN_getOutputValue(Board_LED0));
        Task_sleep(100000);


        /* Scale values to be centered at 0, ie no thrust at neutral position */
        leftXAvg = (leftXMax + leftXMin) / 2;
        leftYAvg = (leftYMax + leftYMin) / 2;
        rightXAvg = (rightXMax + rightXMin) / 2;
        rightYAvg = (rightYMax + rightYMin) / 2;
        leftXMax -= leftXAvg;
        leftXMin -= leftXAvg;
        leftYMax -= leftYAvg; 
        leftYMin -= leftYAvg; 
        rightXMax -= rightXAvg; 
        rightXMin -= rightXAvg;
        rightYMax -= rightYAvg; 
        rightYMin -= rightYAvg; 

        /* Begin Retrieving Data */
        Clock_stop(clk0Handle);

        /* Begin RF Transmission */
        Semaphore_post(rfRdySem);
    }
}

/*
 *  ======== controllerFxn ========
 *  Uses four ADC channels to sample the controllers
 *  left and right joysticks.
 */
Void controllerFxn(UArg arg0, UArg arg1)
{
    /* ADC conversion result variables */
    adcSampleObj adcData;

    ADC_Handle adcJoyLeftX, adcJoyLeftY, adcJoyRightX, adcJoyRightY;
    ADC_Params params;

    /* Initialize ADC driver */
    ADC_Params_init(&params);
    adcJoyLeftX = ADC_open(Board_ADC_LEFTX, &params);
    adcJoyLeftY = ADC_open(Board_ADC_LEFTY, &params);
    adcJoyRightX = ADC_open(Board_ADC_RIGHTX, &params);
    adcJoyRightY = ADC_open(Board_ADC_RIGHTY, &params);

    while(1) {
        /* Read ADC at specified period */
        Semaphore_pend(sampleSem, BIOS_WAIT_FOREVER);

        ADC_convert(adcJoyLeftX, &adcData.adcChannel[JOYLEFTX]);
        ADC_convert(adcJoyLeftY, &adcData.adcChannel[JOYLEFTY]);
        ADC_convert(adcJoyRightX, &adcData.adcChannel[JOYRIGHTX]);
        ADC_convert(adcJoyRightY, &adcData.adcChannel[JOYRIGHTY]);

        /* Post message containing current raw data samples */
        Mailbox_post(mbxHandle, &adcData, BIOS_WAIT_FOREVER);
    }
}

/*
 *  ======== main ========
 */
int main(void)
{
    Task_Params taskParams;
    Clock_Params clkParams;
    Semaphore_Params semParams;
    Mailbox_Params mbxParams;

    /* Call board init functions */
    Board_initGeneral();
    Board_initADC();

    /* Construct joystick sampling task */
    Task_Params_init(&taskParams);
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    taskParams.priority = 11;
    Task_construct(&task0Struct, (Task_FuncPtr)controllerFxn, &taskParams, NULL);

    /* Joystick calibration Task */
    Task_Params_init(&taskParams);
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task1Stack;
    Task_construct(&task1Struct, (Task_FuncPtr)calibrationFxn, &taskParams, NULL);

    /* Construct joystick sampling task */
    Task_Params_init(&taskParams);
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task2Stack;
    Task_construct(&task2Struct, (Task_FuncPtr)RFTransmitFxn, &taskParams, NULL);

    /* Construct a Mailbox Instance to send raw joystick data */
    Mailbox_Params_init(&mbxParams);
    mbxParams.buf = mailboxBuf;
    mbxParams.bufSize = MAILBOXSIZE;
    Mailbox_construct(&mbxStruct, sizeof(adcSampleObj), NUMMBXELEMS, &mbxParams, NULL);
    mbxHandle = Mailbox_handle(&mbxStruct);

    Clock_Params_init(&clkParams);

    clkParams.period = SAMPLERATETICKS;
    clkParams.startFlag = FALSE;

    /* Construct periodic sampling clock */
    Clock_construct(&clk0Struct, (Clock_FuncPtr)periodicSampleFxn,
                    SAMPLERATETICKS, &clkParams);

    clk0Handle = Clock_handle(&clk0Struct);

    /* Construct GPIO debounce clock for Button0 */
    clkParams.period = 0;
    clkParams.startFlag = FALSE;
    Clock_construct(&clk1Struct, (Clock_FuncPtr)gpioDebounceFxn0,
    		DEBOUNCETICKS, &clkParams);

    clk1Handle = Clock_handle(&clk1Struct);

    /* Construct GPIO debounce clock for Button1 */
    clkParams.period = 0;
    clkParams.startFlag = FALSE;
    Clock_construct(&clk2Struct, (Clock_FuncPtr)gpioDebounceFxn1,
                    DEBOUNCETICKS, &clkParams);

    clk2Handle = Clock_handle(&clk2Struct);

    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;

    /* Semaphore to unblock task to sample the joystick/send data over RF */
    Semaphore_construct(&sampleStruct, 0, &semParams);
    sampleSem = Semaphore_handle(&sampleStruct);

    /* Semaphore to activate the calibration Task */
    Semaphore_construct(&calibStruct, 0, &semParams);
    calibSem = Semaphore_handle(&calibStruct);

    /* Semaphore to activate the RF Transmit Task */
    Semaphore_construct(&rfRdyStruct, 0, &semParams);
    rfRdySem = Semaphore_handle(&rfRdyStruct);

    /* Open LED pins */
    ledPinHandle = PIN_open(&ledPinState, ledPinTable);
    if(!ledPinHandle) {
        System_abort("Error initializing board LED pins\n");
    }

    buttonPinHandle = PIN_open(&buttonPinState, buttonPinTable);
    if(!buttonPinHandle) {
        System_abort("Error initializing button pins\n");
    }

    /* Setup callback for button pins */
    if (PIN_registerIntCb(buttonPinHandle, &buttonCallbackFxn) != 0) {
        System_abort("Error registering button callback function");
    }

    /* Start kernel */
    BIOS_start();

    return (0);
}

/*
 *  ======== gpioDebounceFxn0 ========
 *  Debounce function for the GPIO interrupts.
 */
Void gpioDebounceFxn0(UArg arg0)
{
    /* Begin Calibration routine */
    Semaphore_post(calibSem);

    /* Toggle LED based on the button pressed */
    PIN_setOutputValue(ledPinHandle, Board_LED0, !PIN_getOutputValue(Board_LED0));
}

/*
 *  ======== gpioDebounceFxn1 ========
 *  Debounce function for the GPIO interrupts. This data is sent to the receiving CC1350.
 */
Void gpioDebounceFxn1(UArg arg0)
{
	buttonPressed = 0x01;
    /* Toggle LED based on the button pressed */
    PIN_setOutputValue(ledPinHandle, Board_LED1, !PIN_getOutputValue(Board_LED1));
}

/* Function to sample adc and read both analog joysticks */
Void periodicSampleFxn(UArg arg0)
{
    Semaphore_post(sampleSem);
}

/*
 *  ======== buttonCallbackFxn ========
 *  Pin interrupt Callback function board buttons configured in the pinTable.
 *  If Board_LED3 and Board_LED4 are defined, then we'll add them to the PIN
 *  callback function.
 */
void buttonCallbackFxn(PIN_Handle handle, PIN_Id pinId) {

    if (!PIN_getInputValue(pinId)) {
        /* Toggle LED based on the button pressed */
        switch (pinId) {
            case Board_BUTTON0:
                /* Restart the clock */
                Clock_start(clk1Handle); 
                break;

            case Board_BUTTON1:
                /* Restart the clock */
                Clock_start(clk2Handle); 
                break;

            default:
                /* Do nothing */
                break;
        }
    }
}
