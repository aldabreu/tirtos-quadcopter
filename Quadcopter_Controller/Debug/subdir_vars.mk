################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../CC1350_LAUNCHXL.cmd 

CFG_SRCS += \
../quadcopterController.cfg 

C_SRCS += \
../CC1350_LAUNCHXL.c \
../Display.c \
../DisplayUart.c \
../ccfg.c \
../quadcopterController.c 

OBJS += \
./CC1350_LAUNCHXL.obj \
./Display.obj \
./DisplayUart.obj \
./ccfg.obj \
./quadcopterController.obj 

C_DEPS += \
./CC1350_LAUNCHXL.pp \
./Display.pp \
./DisplayUart.pp \
./ccfg.pp \
./quadcopterController.pp 

GEN_MISC_DIRS += \
./configPkg/ 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_OPTS += \
./configPkg/compiler.opt 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

GEN_FILES__QUOTED += \
"configPkg\linker.cmd" \
"configPkg\compiler.opt" 

GEN_MISC_DIRS__QUOTED += \
"configPkg\" 

C_DEPS__QUOTED += \
"CC1350_LAUNCHXL.pp" \
"Display.pp" \
"DisplayUart.pp" \
"ccfg.pp" \
"quadcopterController.pp" 

OBJS__QUOTED += \
"CC1350_LAUNCHXL.obj" \
"Display.obj" \
"DisplayUart.obj" \
"ccfg.obj" \
"quadcopterController.obj" 

C_SRCS__QUOTED += \
"../CC1350_LAUNCHXL.c" \
"../Display.c" \
"../DisplayUart.c" \
"../ccfg.c" \
"../quadcopterController.c" 


